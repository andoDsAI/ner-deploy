import logging
import os

import numpy as np
import torch
from preprocessing import preprocessing
from utils import MODEL_CLASSES, get_slot_labels, init_logger, load_tokenizer

logger = logging.getLogger(__name__)


def get_device():
    return "cuda" if torch.cuda.is_available() else "cpu"


def get_args(model_dir):
    return torch.load(os.path.join(model_dir, "training_args.bin"))


def load_model(model_dir, args, slot_label_lst):
    # Check whether model exists
    if not os.path.exists(model_dir):
        raise Exception("Model doesn't exists! Train first!")
    try:
        _, model_class, _ = MODEL_CLASSES[args.model_type]
        model = model_class.from_pretrained(
            model_dir,
            args=args,
            slot_label_lst=slot_label_lst,
        )
        logger.info("***** Model Loaded *****")
    except Exception:
        raise Exception("Some model files might be missing...")

    return model


def convert_input_to_tensor(
    sentence,
    args,
    tokenizer,
    cls_token_segment_id=0,
    pad_token_segment_id=0,
    sequence_a_segment_id=0,
    mask_padding_with_zero=True,
):
    # Setting based on the current model type
    cls_token = tokenizer.cls_token
    sep_token = tokenizer.sep_token
    unk_token = tokenizer.unk_token
    pad_token_id = tokenizer.pad_token_id

    tokens = []
    slot_label_mask = []
    for word in sentence:
        word_tokens = tokenizer.tokenize(word)
        if not word_tokens:
            word_tokens = [unk_token]  # For handling the bad-encoded word
        tokens.extend(word_tokens)
        # Use the real label id for the first token of the word, and padding ids for the remaining tokens
        slot_label_mask.extend(
            [pad_token_label_id + 1] + [pad_token_label_id] * (len(word_tokens) - 1)
        )

    # Account for [CLS] and [SEP]
    special_tokens_count = 2
    if len(tokens) > args.max_seq_len - special_tokens_count:
        tokens = tokens[: (args.max_seq_len - special_tokens_count)]
        slot_label_mask = slot_label_mask[: (args.max_seq_len - special_tokens_count)]

    # Add [SEP] token
    tokens += [sep_token]
    token_type_ids = [sequence_a_segment_id] * len(tokens)
    slot_label_mask += [pad_token_label_id]

    # Add [CLS] token
    tokens = [cls_token] + tokens
    token_type_ids = [cls_token_segment_id] + token_type_ids
    slot_label_mask = [pad_token_label_id] + slot_label_mask

    input_ids = tokenizer.convert_tokens_to_ids(tokens)

    # The mask has 1 for real tokens and 0 for padding tokens. Only real tokens are attended to.
    attention_mask = [1 if mask_padding_with_zero else 0] * len(input_ids)

    # Zero-pad up to the sequence length.
    padding_length = args.max_seq_len - len(input_ids)
    input_ids = input_ids + ([pad_token_id] * padding_length)
    attention_mask = attention_mask + ([0 if mask_padding_with_zero else 1] * padding_length)
    token_type_ids = token_type_ids + ([pad_token_segment_id] * padding_length)
    slot_label_mask = slot_label_mask + ([pad_token_label_id] * padding_length)

    # Change to Tensor
    input_ids = torch.tensor([input_ids], dtype=torch.long)
    attention_mask = torch.tensor([attention_mask], dtype=torch.long)
    token_type_ids = torch.tensor([token_type_ids], dtype=torch.long)
    slot_label_mask = torch.tensor([slot_label_mask], dtype=torch.long)

    return {
        "input_ids": input_ids,
        "attention_mask": attention_mask,
        "token_type_ids": token_type_ids,
        "slot_label_mask": slot_label_mask,
    }


def get_annotation(sentence):
    # Convert input file to TensorDataset
    sentence = preprocessing(sentence)
    sentence = sentence.split()
    feature = convert_input_to_tensor(sentence, args, tokenizer)

    # Predict
    inputs = {
        "input_ids": feature["input_ids"].to(device),
        "attention_mask": feature["attention_mask"].to(device),
        "slot_labels_ids": None,
    }
    if args.model_type != "distilbert":
        inputs["token_type_ids"] = feature["token_type_ids"].to(device)

    outputs = model(**inputs)
    slot_logits = outputs

    if args.use_crf:
        slot_preds = np.array(model.crf.decode(slot_logits)).flatten()
    else:
        slot_preds = slot_logits.detach().cpu().numpy()
        slot_preds = np.argmax(slot_preds, axis=1).flatten()

    slot_preds_list = []
    slot_label_mask = feature["slot_label_mask"].detach().cpu().numpy().flatten()
    for i in range(slot_preds.shape[0]):
        if slot_label_mask[i] != pad_token_label_id:
            slot_preds_list.append(id2label[slot_preds[i]])

    # tag I impossible start
    for i in range(len(slot_preds_list)):
        if slot_preds_list[i].startswith("I-"):
            if i == 0:
                slot_preds_list[i] = slot_preds_list[i].replace("I-", "B-")
            else:
                if (
                    slot_preds_list[i - 1] == "O"
                    or slot_preds_list[i - 1][2:] != slot_preds_list[i][2:]
                ):
                    slot_preds_list[i] = slot_preds_list[i].replace("I-", "B-")

    return_list = []
    for i in range(len(slot_preds_list)):
        if slot_preds_list[i] != "O":
            return_list.append((sentence[i], slot_preds_list[i]))
        else:
            return_list.append(sentence[i])

    return return_list


init_logger()
model_dir = "trained_models"

slot_label_lst = get_slot_labels("slot_label.txt")
id2label = {i: label for i, label in enumerate(slot_label_lst)}
label2id = {label: i for i, label in enumerate(slot_label_lst)}

args = get_args(model_dir)

pad_token_label_id = args.ignore_index
tokenizer = load_tokenizer(args)

model = load_model(model_dir, args, slot_label_lst)
device = get_device()
model.to(device)
model.eval()
