import torch.nn as nn
import torch.nn.functional as F
from torchcrf import CRF
from transformers.models.roberta.modeling_roberta import RobertaModel, RobertaPreTrainedModel
from transformers.models.xlm_roberta.modeling_xlm_roberta import XLMRobertaModel

from .module import RuleProcessor, SlotClassifier


class NerBert(RobertaPreTrainedModel):
    def __init__(self, config, args, slot_label_lst):
        super(NerBert, self).__init__(config)
        self.args = args
        self.num_slot_labels = len(slot_label_lst)

        if args.model_type == "phobert":
            self.roberta = RobertaModel(config)  # Load pretrained phoBERT
        else:
            self.roberta = XLMRobertaModel(config)

        self.dropout = nn.Dropout(args.dropout_rate)

        self.slot_classifier = SlotClassifier(
            input_dim=config.hidden_size,
            num_slot_labels=self.num_slot_labels,
            hidden_size=args.attention_embedding_size,
            dropout_rate=args.dropout_rate,
        )
        if args.use_crf:
            self.crf = CRF(num_tags=self.num_slot_labels, batch_first=True)

        # rule_processor = RuleProcessor()
        # self.init_crf_transitions(rule_processor, slot_label_lst=slot_label_lst)

    def init_crf_transitions(self, rule_processor: RuleProcessor, slot_label_lst, imp_value=-1e4):
        """
        :param tag_list: ['O', "B-dia_chi', ...]
        :param imp_value: value that we assign for impossible transition, ex : B-dia_chi -> I-sdt
        """
        rule_processor.process(self.crf, slot_label_lst, imp_value)

    def forward(self, input_ids, attention_mask, token_type_ids, slot_labels_ids):
        outputs = self.roberta(
            input_ids=input_ids,
            attention_mask=attention_mask,
        )

        sequence_output = outputs[0]
        sequence_output = self.dropout(sequence_output)
        slot_logits = self.slot_classifier(sequence_output)
        # slot_logits = F.log_softmax(slot_logits, dim=-1)

        total_loss = 0
        if slot_labels_ids is not None:
            if self.args.use_crf:
                slot_loss = self.crf(
                    slot_logits,
                    slot_labels_ids,
                    mask=attention_mask.byte(),
                    reduction="token_mean",
                )
                slot_loss = -1 * slot_loss  # negative log-likelihood
            else:
                slot_loss_fct = nn.CrossEntropyLoss(ignore_index=self.args.ignore_index)
                # Only keep active parts of the loss
                if attention_mask is not None:
                    active_loss = attention_mask.view(-1) == 1
                    active_logits = slot_logits.view(-1, self.num_slot_labels)[active_loss]
                    active_labels = slot_labels_ids.view(-1)[active_loss]
                    slot_loss = slot_loss_fct(active_logits, active_labels)
                else:
                    slot_loss = slot_loss_fct(
                        slot_logits.view(-1, self.num_slot_labels), slot_labels_ids.view(-1)
                    )
            total_loss += slot_loss
            return total_loss, slot_logits
        else:
            return slot_logits

    def post_predict(self, labels):
        """Process label to remove ignored labels"""
        true_labels_step = [
            [label for label in sent_label[1:] if label != self.args.ignore_index]
            for sent_label in labels.tolist()
        ]
        return true_labels_step
