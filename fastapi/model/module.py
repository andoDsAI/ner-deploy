import torch.nn as nn
from torchcrf import CRF


class SlotClassifier(nn.Module):
    def __init__(
        self,
        input_dim,
        num_slot_labels,
        hidden_size=768,
        dropout_rate=0.4,
        head_init_range=0.04,
    ):
        super(SlotClassifier, self).__init__()
        self.input_dim = input_dim
        self.hidden_size = hidden_size
        self.num_slot_labels = num_slot_labels

        self.lstm = nn.LSTM(
            input_size=input_dim,
            hidden_size=hidden_size,
            batch_first=True,
            num_layers=2,
            bidirectional=True,
            dropout=dropout_rate,
        )
        self.dropout = nn.Dropout(dropout_rate)
        self.linear = nn.Linear(hidden_size * 2, num_slot_labels)
        # initializing classification
        self.linear.weight.data.normal_(mean=0.0, std=head_init_range)

    def forward(self, x):
        lstm_out, _ = self.lstm(x)
        x = self.dropout(lstm_out)
        return self.linear(x)


class RuleProcessor:
    """Class to specify the CRF rules"""

    def process(self, crf: CRF, slot_label_lst, imp_value=-1e4):
        num_labels = len(slot_label_lst)
        for i in range(num_labels):
            # Rule 1 : I label is impossible to be start label
            if slot_label_lst[i].startswith("I-"):
                nn.init.constant_(crf.start_transitions[i], imp_value)

        # Rule 2 : I label is only followed by I label or B label of same type
        for i in range(num_labels):
            for j in range(num_labels):
                if slot_label_lst[j].startswith("I-") and (
                    slot_label_lst[i][2:] != slot_label_lst[j][2:]
                ):
                    nn.init.constant_(crf.transitions[i, j], imp_value)
