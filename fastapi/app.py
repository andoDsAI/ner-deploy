import uvicorn
from service import get_annotation

from fastapi import FastAPI

app = FastAPI()


@app.post("/ner")
def do_ner(text: str):
    return {"text": get_annotation(text)}


if __name__ == "__main__":
    uvicorn.run("app:app", host="0.0.0.0", port=8000)
