import logging
import os
import random

import numpy as np
import torch
from model import NerBert
from transformers import AutoTokenizer, RobertaConfig, XLMRobertaConfig, XLMRobertaTokenizerFast

MODEL_CLASSES = {
    "xlmr": (XLMRobertaConfig, NerBert, XLMRobertaTokenizerFast),
    "phobert": (RobertaConfig, NerBert, AutoTokenizer),
}

MODEL_PATH_MAP = {
    "xlmr": "xlm-roberta-base",
    "phobert": "vinai/phobert-base",
}


def get_slot_labels(path):
    with open(path, "r") as f:
        slot_label_lst = [row.replace("\n", "") for row in f.readlines()]
        f.close()
    return slot_label_lst


def load_tokenizer(args):
    return MODEL_CLASSES[args.model_type][2].from_pretrained(args.model_name_or_path)


def init_logger():
    logging.basicConfig(
        format="%(asctime)s - %(levelname)s - %(name)s -   %(message)s",
        datefmt="%m/%d/%Y %H:%M:%S",
        level=logging.INFO,
    )


def seed_everything(args):
    random.seed(args.seed)
    np.random.seed(args.seed)
    torch.manual_seed(args.seed)
    if not args.no_cuda and torch.cuda.is_available():
        torch.cuda.manual_seed_all(args.seed)
