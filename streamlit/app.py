import requests
from annotated_text import annotated_text

import streamlit as st


def name_entity_recognition(message):
    url = "http://localhost:8000/ner?text="
    rsp = requests.post(url + message)
    rsp = rsp.json()
    a = [" " + i + " " if isinstance(i, str) else tuple(i) for i in rsp["text"]]
    return annotated_text(*a)


def main():
    """NLP NER App"""
    st.title("BDS NER")
    st.subheader("Named Entity Recognition")
    message = st.text_area("Input here", "", key="123")
    if st.button("Run", key="start_normal"):
        # st.text_area("Result", name_entity_recognition(message))
        name_entity_recognition(message)


if __name__ == "__main__":
    main()
